<?php
    
/**
 * @param int $rus
 * @return array
 */
function getMorseAlphabetCodes(int $rus)
{
    $morse_codes_json = file_get_contents('MorseCodes.json');
    $codes = json_decode($morse_codes_json, true);
    if (!$rus) {
        $response = array_merge($codes['eng'], $codes['sym']);
    } else {
        $response = array_merge($codes['rus'], $codes['sym']);
    }
    return $response;
}

/**
 * @param string $str
 * @param int $rus
 * @return string
 */
function dencodeMorse(string $str, int $rus = 0)
{
    try {
        $result = "";
        $morse_symbols = getMorseAlphabetCodes($rus);
        $words = explode("   ", $str);
        foreach ($words as $word) {
            $symbols = explode(" ", $word);
            foreach ($symbols as $char) {
                if (isset($morse_symbols[$char])) {
                    $result .= $morse_symbols[$char];
                } else {
                    throw new Exception('Invalid code is provided');
                }
            }
            $result .= " ";
        }
        return trim($result);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

echo dencodeMorse(".... . .-.. .-.. ---   .-- --- .-. .-.. -..");
    
?>
